package br.com.albertoferes.pi.proginternet.util.conversor;

import br.com.albertoferes.pi.proginternet.domain.Usuario;
import br.com.albertoferes.pi.proginternet.domain.UsuarioDTO;
import br.com.albertoferes.pi.proginternet.util.EntityMapper;

//@Mappper
public interface ConversorUsuario extends EntityMapper<UsuarioDTO, Usuario> {

}
