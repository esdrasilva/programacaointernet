package br.com.albertoferes.pi.proginternet.domain;

import java.io.Serializable;

public class UsuarioDTO implements Serializable {
  private static final long serialVersionUID =1L;
  private Integer id;
  private String nome;
  private String email;

  public Integer getId() {
    return id;
  }

  public UsuarioDTO setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getNome() {
    return nome;
  }

  public UsuarioDTO setNome(String nome) {
    this.nome = nome;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public UsuarioDTO setEmail(String email) {
    this.email = email;
    return this;
  }
}
