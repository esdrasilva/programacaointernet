package br.com.albertoferes.pi.proginternet.util;

import com.google.common.base.Strings;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;

/**
 * Contrato generico para Mappers
 *
 * @param <D> - tipo do DTO.
 * @param <E> - tipo da Entidade.
 */

public interface EntityMapper<D, E> {

    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntity(List<D> dtoList);

    List<D> toDto(List<E> entityList);

    default Blob map(byte[] value) throws SQLException {
        return Objects.isNull(value) ? null : new SerialBlob(value);
    }

    default byte[] map(Blob value) throws SQLException {
        return Objects.isNull(value) ? null : value.getBytes(1, Math.toIntExact(value.length()));
    }

    default Clob map(String value) throws SQLException {
        return Strings.isNullOrEmpty(value) ? null : new SerialClob(value.toCharArray());
    }

    default String map(Clob value) throws SQLException {
        return Objects.isNull(value) ? null : value.getSubString(1, Math.toIntExact(value.length()));
    }
}
