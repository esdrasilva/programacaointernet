package br.com.albertoferes.pi.proginternet.controllers;

import br.com.albertoferes.pi.proginternet.domain.Usuario;
import br.com.albertoferes.pi.proginternet.repositories.UsuarioRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UsuarioController {

  private static final String INDEX = "index";
  private static final String LISTA_USUARIOS = "usuarios";

    UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioController(
        UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @GetMapping("/cadastrar")
    public String exibirFormularioLogin(Usuario usuario){
        return "adicionar-usuario";
    }

    @GetMapping("/")
    public String home(Model model){
      List<Usuario> usuarios = usuarioRepository.findAll();
      model.addAttribute(LISTA_USUARIOS, usuarios);
      return INDEX;
    }

    @PostMapping("/adicionar")
    public String adicionarUsuario(@Valid Usuario usuario, BindingResult result, Model model){
        if (result.hasErrors()){
            return "adicionar-usuario";
        }
        usuarioRepository.save(usuario);
        model.addAttribute(LISTA_USUARIOS, usuarioRepository.findAll());
        return INDEX;
    }

    @GetMapping("/editar/{id}")
    public String exibirFormularioEdicao(@PathVariable("id") int id, Model model){
        Usuario usuario = usuarioRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Id invalido: "+id));
        model.addAttribute("usuario",usuario);
        return "atualizar-usuario";
    }

    @PostMapping("/atualizar/{id}")
    public String atualizarUsuario(@PathVariable("id") int id, @Valid Usuario usuario, BindingResult result, Model model){
        if (result.hasErrors()){
            return "atualizar-usuario";
        }
        usuarioRepository.save(usuario);
        model.addAttribute(LISTA_USUARIOS, usuarioRepository.findAll());
        return INDEX;
    }

    @GetMapping("/apagar/{id}")
    public String apagarUsuario(@PathVariable("id") int id, Model model){
        Usuario usuario = usuarioRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Id invalido: "+id));
        usuarioRepository.delete(usuario);
        model.addAttribute(LISTA_USUARIOS, usuarioRepository.findAll());
        return INDEX;
    }
}
