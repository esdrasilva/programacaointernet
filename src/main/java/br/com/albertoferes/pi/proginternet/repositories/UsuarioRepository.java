package br.com.albertoferes.pi.proginternet.repositories;

import br.com.albertoferes.pi.proginternet.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
}
